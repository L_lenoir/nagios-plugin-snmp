#!/bin/bash

ipTarget=$1
comunity="public"
info="IF-MIB::ifOutOctets.2"

sleep_time=10

normal_quota=2000
danger_quota=5000

firstMesure=$(snmpget -v 2c -c $comunity $ipTarget $info | cut -d " " -f 4)
sleep $sleep_time
lastMesure=$(snmpget -v 2c -c $comunity $ipTarget $info | cut -d " " -f 4)

delta=$(($lastMesure - $firstMesure))
outTrafic=$(expr $delta / $sleep_time)

if (($outTrafic < $normal_quota)); then
    echo "OK - out trafic = $outTrafic"
    exit 0
elif (($outTrafic > $normal_quota && $outTrafic<$danger_quota)); then
    echo "WARNING - out trafic = $outTrafic"
    exit 1
elif (($outTrafic > $danger_quota)); then
    echo "CRITICAL - out trafic = $outTrafic"
    exit 2
else
    echo "UNKNOW - out trafic = $outTrafic"
    exit 3
fi