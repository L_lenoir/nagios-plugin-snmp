# Rapport combinaison Nagios et SNMP

> DROUES Philemon 

> LENOIR Luc

## Introduction

L'objectif de ce TP est de réalisé un plugin permettant de surveiller la bande passante sortante du trafic.

Il nécessite l'usage de VM Linux.

### Prérequis

2 machines sur un même réseau sui peuvent communiquer et qui sont en adressage statique

sur les 2 machines assurez vous d'avoir installé le MIBs
<details>
<summary>Tuto</summary>
1. ajoutez les depots non-free

modifiez le fichier `/etc/apt/sources.list` et à la fin des lignes décomenté ajoutez `non-free`

2. installez le paquet `snmp-mibs-downloader`

```bash
apt update && apt install -y snmp-mibs-downloader
```

3. Télécharger la définition des MIBs

Editer le fichier /etc/snmp/snmp.conf et commenter la ligne mibs puis exécuter

```bash
download-mibs
```
</details>

puis assurez-vous de pouvoir lire la valeur voulue
<details>
<summary>Tuto</summary>
dans le fichier `/etc/snmp/snmpd.conf` ajouter les liges suivantes:

```
view    all included    .1
rocommunity public default -V all
```

*/!\\ la methode suivante "desactive" la sécurité*
</details>

## Formule mathématique

La formule permettant d'obtenir la bande passante sortante est la suivante : 

$$
Δ ifOutOctets/temps
$$

- Oû *Δ ifOutOctets* est égal a la différence entre les deux mesures effectué sur l’objet ifOutOcets.
- Oû *temps* est égal au temps en seconde écoulé entre les deux mesures.

## Script

Le script permettant d’implémenter la formule mathématique précédente et d’interpréter le résultat est le suivant, réalisé en langage shell :

```bash
#!/bin/bash
ipTarget=$1 # ip de la cible se passe en argument pour etre plus modulable
comunity="public"
info="IF-MIB::ifOutOctets.2"

sleep_time=10

normal_quota=2000
danger_quota=5000

firstMesure=$(snmpget -v 2c -c $comunity $ipTarget $info | cut -d " " -f 4)
sleep $sleep_time
lastMesure=$(snmpget -v 2c -c $comunity $ipTarget $info | cut -d " " -f 4)

delta=$(($lastMesure - $firstMesure))
outTrafic=$(expr $delta / $sleep_time)

if (($outTrafic < $normal_quota)); then
    echo "OK - out trafic = $outTrafic"
    exit 0
elif (($outTrafic > $normal_quota && $outTrafic<$danger_quota)); then
    echo "WARNING - out trafic = $outTrafic"
    exit 1
elif (($outTrafic > $danger_quota)); then
    echo "CRITICAL - out trafic = $outTrafic"
    exit 2
else
    echo "UNKNOW - out trafic = $outTrafic"
    exit 3
fi
```

Le script récupère le débit grâce a la commande snmp suivante :

```bash
snmpget -v 2c -c public 10.0.2.11 IF-MIB::ifOutOctets.2
```

La formule mathématique décrite précédemment est ensuite appliqué au résultats des mesures afin d'obtenir la bande passante, suite a quoi le résultat est interprété et afficher a l'aide d'une structure else if.

## Mise en place du plugin

placez le script dans `/etc/usr/local/nagios/libexec/<script>.sh`, donnez lui les permission adéquate (l'utilisateur nagios doit pouvoir l’exécuter)

ajoutez sa définition dans le fichier `/usr/local/nagios/etc/objects/commands.cfg` comme ci dessous:

```bash
define command {
        command_name    check_out_network
        command_line    /usr/local/nagios/libexec/check_output_trafic.sh $HOSTADDRESS$
}
```

puis ajoutez le check au serveur que vous voulez surveiller dans `/usr/local/nagios/etc/servers/<serv>.cfg`:

```bash
define service {
      use local-service
      host_name                       srv-deb-apache2
      service_description             Check output network
      check_command                   check_out_network
      max_check_attempts              2
      check_interval                  2
      retry_interval                  2
      check_period                    24x7
      check_freshness                 1
      contact_groups                  admins
      notification_interval           2
      notification_period             24x7
      notifications_enabled           1
      register                        1
}
```


